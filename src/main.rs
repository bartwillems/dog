extern crate rand;

use rand::seq::SliceRandom;
use rand::thread_rng;
use rand::Rng;
use std::env;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

static DOGGO_NOISES: [&str; 6] = [
    "WOOF!!",
    "BARK!!",
    "RUFF!!",
    "ARF!!",
    "HRRRRRRR....!!!!",
    "ERF!!!",
];

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args: Vec<String> = env::args().collect();
    let input_file = parse_config(&args)?;

    let file = File::open(&input_file)?;

    let mut rng = thread_rng();

    let reader = BufReader::new(file);

    for line in reader.lines() {
        match line {
            Ok(line) => {
                // println!("{}", line);
                let dink = insert_dog_noise(line.as_ref(), &mut rng);
                println!("{}", dink);
            }
            Err(e) => return Err(Box::from(e)),
        }
    }

    Ok(())
}

fn parse_config(args: &[String]) -> Result<String, &'static str> {
    if args.len() < 2 {
        return Err("WOOF WOOF!!! input file not provided. BARK!!!!");
    }

    Ok(args[1].clone())
}

fn get_dog_noise(rng: &mut rand::prelude::ThreadRng) -> &'static str {
    return DOGGO_NOISES.choose(rng).unwrap();
}

fn insert_dog_noise(input: &str, rng: &mut rand::prelude::ThreadRng) -> String {
    let mut dink = String::from(input);

    let len = input.len();

    if len == 0 {
        return dink;
    }

    dink.insert_str(rng.gen_range(0, len), get_dog_noise(rng));

    dink
}
