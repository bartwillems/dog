# dog

a modern rewrite of `gnu/cat` in rust

## example usage

``` bash
$ dog Cargo.toml
[packRUFF!!age]
name = "dogHRRRRRRR....!!!!"
version = HRRRRRRR....!!!!"0.1.0"
authors = ["bart <bwillemERF!!!s@protonmail.com>"]
edition = "2018ERF!!!"

# See more keys and their definitions at https://doc.rust-lang.org/cargo/reference/mBARK!!anifest.html

[dependenciRUFF!!es]
rand = ERF!!!"0.7"
```